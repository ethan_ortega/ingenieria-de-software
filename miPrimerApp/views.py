from django.shortcuts import render, HttpResponse
from .models import *

# Create your views here.
def index(request):
    grupo1 = Estudiante.objects.filter(grupo = 1)
    grupo4 = Estudiante.objects.filter(grupo = 4)
    apellido = Estudiante.objects.filter(apellidos = "Gómez")
    edad = Estudiante.objects.filter(edad = 22)
    grupo_edad = Estudiante.objects.filter(grupo = 3, edad = 22)
    todos = Estudiante.objects.filter()

    context = {
        'grupo1': grupo1,
        'grupo4': grupo4,
        'todos': todos,
        'apellido': apellido,
        'edad': edad,
        'grupo_edad': grupo_edad,
    }

    return render(request, 'index.html', context)
